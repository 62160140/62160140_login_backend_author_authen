// !Authentication = การพิสูจว่าใคร
const express = require('express')
const router = express.Router()
const User = require('../models/User')
const { generateAccessToken } = require('../helpers/auth')

/* ---------------------- get by id --------------------- */
const login = async function (req, res, next) {
  // ต้อง getUser
  const username = req.body.username
  const password = req.body.password

  try {
    const user = await User.findOne({ username: username, password: password }, '-password').exec()
    /* ------------------- generate token ------------------- */
    const token = generateAccessToken({ username: user.username, roles: user.roles })
    /* ------------------- End generate token ------------------- */
    /* ------------------------ debug ----------------------- */
    res.status(200).json(
      {
        user: user,
        token: token
      })
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

/* --------------------- add User -------------------- */
router.post('/', login)

module.exports = router
