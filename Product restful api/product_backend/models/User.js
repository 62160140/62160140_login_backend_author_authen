const { ROLE } = require('../routes/constant.js')
const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  username: String,
  password: String, // จริงๆต้องเป็น  plain/text
  roles: {
    type: [String], // Array type string
    default: [ROLE.USER]
  }
})

module.exports = mongoose.model('User', userSchema)
