const mongoose = require('mongoose')
const User = require('../models/User')
mongoose.connect('mongodb://localhost:27017/example')
const { ROLE } = require('../routes/constant.js')

async function main () {
  await clearProduct()
  //! สร้างข้อมูล และบันทึกลง collection Product
  const user = new User({ username: 'user', password: 'password', roles: [ROLE.USER] })
  user.save()
  const admin = new User({ username: 'admin', password: 'password', roles: [ROLE.ADMIN] })
  admin.save()
}
//! function clear DB
async function clearProduct () {
  await User.deleteMany({})
}

main().then(() => console.log('Finish'))
