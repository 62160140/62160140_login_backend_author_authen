
const jwt = require('jsonwebtoken')

const generateAccessToken = function (user) {
  return jwt.sign(user, process.env.TOKEN_SECRET, { expiresIn: '1d' })
}

/* ------------- authen a token [middleware] ------------ */
const authenMiddleware = function (req, res, next) {
  // ! check จาก header
  const authHeader = req.headers.authorization
  // มี authenHeader หรือป่าว && split แล้วเอาตัวที่ 1
  const token = authHeader && authHeader.split(' ')[1]
  // ถ้า token == null
  if (token === null) return res.sendStatus(401)
  jwt.verify(token, process.env.TOKEN_SECRET, function (err, user) {
    console.log(err)
    if (err) return res.sendStatus(403)
    req.user = user
    next()
  })
}

const authorizeMiddleware = function (roles) {
  return function (req, res, next) {
    //! เช็คว่าเป็น adminไหม
    console.log(roles.length)
    for (let i = 0; i < roles.length; i++) {
      // ? เจอ
      console.log(req.user.roles.indexOf(roles[i]))
      if (req.user.roles.indexOf(roles[i]) >= 0) {
        console.log('true')
        next()
        return
      }
    }
    res.sendStatus(401)
  }
}

module.exports = { generateAccessToken, authenMiddleware, authorizeMiddleware }
